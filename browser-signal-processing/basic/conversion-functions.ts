// (c) 2015-2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

/**
 * Convert between samples and time, given a certain sample rate.
 *
 * @param samples Source value (unitless).
 * @param sampleRate The sample rate used for conversion.
 * @returns Target value (in seconds).
 */
export function samplesToSeconds(samples:number, sampleRate:number):number {
	return samples / sampleRate;
}
/**
 * Convert between samples and time, given a certain sample rate.
 *
 * @param samples Source value (unitless).
 * @param sampleRate The sample rate used for conversion.
 * @returns Target value (in millisecnds).
 */
export function samplesToMilliseconds(samples:number, sampleRate:number):number {
	return samples / sampleRate * 1000;
}
/**
 * Convert between samples and time, given a certain sample rate.
 *
 * @param seconds Source value (in seconds).
 * @param sampleRate The sample rate used for conversion.
 * @returns Rounded target value (unitless).
 */
export function secondsToSamples(seconds:number, sampleRate:number):number {
	return Math.round(sampleRate * seconds);
}
/**
 * Convert between samples and time, given a certain sample rate.
 *
 * @param milliseconds Source value (in milliseconds).
 * @param sampleRate The sample rate used for conversion.
 * @returns Rounded target value (unitless).
 */
export function millisecondsToSamples(milliseconds:number, sampleRate:number):number {
	return Math.round(sampleRate * milliseconds / 1000);
}
