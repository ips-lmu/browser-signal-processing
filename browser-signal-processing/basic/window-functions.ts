// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>
// (c) Raphael Winkelmann <raphael@phonetik.uni-muenchen.de>

export function windowBartlett(length, index) {
	return 2 / (length - 1) * ((length - 1) / 2 - Math.abs(index - (length - 1) / 2));
};

export function windowBartlettHann(length, index) {
	return 0.62 - 0.48 * Math.abs(index / (length - 1) - 0.5) - 0.38 * Math.cos(2 * Math.PI * index / (length - 1));
};

export function windowBlackman(length, index, alpha) {
	var a0 = (1 - alpha) / 2;
	var a1 = 0.5;
	var a2 = alpha / 2;
	return a0 - a1 * Math.cos(2 * Math.PI * index / (length - 1)) + a2 * Math.cos(4 * Math.PI * index / (length - 1));
};

export function windowCosine(length, index) {
	return Math.cos(Math.PI * index / (length - 1) - Math.PI / 2);
};

export function windowGauss(length, index, alpha) {
	return Math.pow(Math.E, -0.5 * Math.pow((index - (length - 1) / 2) / (alpha * (length - 1) / 2), 2));
};

export function windowHamming(length, index) {
	return 0.54 - 0.46 * Math.cos(2 * Math.PI * index / (length - 1));
};

export function windowHann(length, index) {
	return 0.5 * (1 - Math.cos(2 * Math.PI * index / (length - 1)));
};

export function windowLanczos(length, index) {
	var x = 2 * index / (length - 1) - 1;
	return Math.sin(Math.PI * x) / (Math.PI * x);
};

export function windowRectangular() {
	return 1;
};

export function windowTriangular(length, index) {
	return 2 / length * (length / 2 - Math.abs(index - (length - 1) / 2));
};
