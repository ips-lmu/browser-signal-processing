// (c) 2015-2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

/**
 * Root mean square.
 *
 * Used to calculate the average energy in a signal. The signal can be
 * passed as an AudioBuffer object, in which case a channel should be
 * defined (defaults to 0). It can also be passed as a number[] or
 * Float32Array, in which case it can only contain one channel.
 *
 * @param signal The signal to analyse.
 * @param channel Which channel to analyse if `signal` is an AudioBuffer object.
 * @returns The root mean square of `signal`.
 */
export function rms(signal:number[]|Float32Array|AudioBuffer, channel:number=0):number {
	if (signal.length === 0) {
		return 0;
	}

	var data: number[]|Float32Array;
	if (signal instanceof AudioBuffer) {
		data = signal.getChannelData(0);
	} else {
		data = signal;
	}

	// Start analysis
	var sum:number = 0;

	for (let i=0; i < data.length; ++i) {
		sum += data[i] * data[i];
	}

	return Math.sqrt(sum / data.length);
}
