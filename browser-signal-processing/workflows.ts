// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {
	g2p,
	G2PInputFormat,
	maus,
	pho2syl,
	Pho2SylOutputFormat
} from "./web-services/bas-services";
import {
	wrasspKsvF0,
	wrasspPitchContourSampleRate
} from "./web-services/wrassp-server";
import {psolaAdjustSignalDuration} from "./advanced/psola";
import {arrayToAudioBuffer} from "./browser-api/format-conversion";
import {asspKsvF0} from "./web-services/assp-http";

/**
 * Run the chain g2p -> maus general -> pho2syl
 *
 * input: recording + orthographic transcription
 * output: segmentation + syllabification
 */
export function syllabify(signal:AudioBuffer | ArrayBuffer | Blob,
                          orthography:string | ArrayBuffer| Blob,
                          language:string,
                          outputFormat:Pho2SylOutputFormat = 'tg',
                          signalSampleRate:number = 0,
                          orthographyInputFormat:G2PInputFormat = 'txt'):Promise<ArrayBuffer> {
	return g2p(orthography, language, 'bpf', orthographyInputFormat)
		.then((transcription) => {
			return maus(signal, transcription, language, 'browser-signal-processing', {
				OUTFORMAT: 'par',
				MODUS: 'align'
			})
				.then((segmentation) => {
					var sampleRate:number;
					if (signal instanceof AudioBuffer) {
						sampleRate = signal.sampleRate;
					} else {
						sampleRate = signalSampleRate;
					}

					return pho2syl(segmentation, language, outputFormat, 'MAU', 'browser-signal-processing', {rate: sampleRate, wsync: 'yes'});
				});
		});
}

export function psolaAdjustSegmentDurations(inputSignal:number[] | Float32Array | AudioBuffer,
                                            originalSegments, //@todo add type
                                            targetSegments, //@todo add tye
                                            sampleRate:number = 0):Promise<AudioBuffer> {
	return new Promise((resolve, reject) => {
		//////////
		// Decode inputSignal if necessary
		var signal:Float32Array;

		if (inputSignal instanceof AudioBuffer) {
			signal = inputSignal.getChannelData(0);
			sampleRate = inputSignal.sampleRate;
		} else if (inputSignal instanceof Float32Array) {
			signal = inputSignal;
		} else {
			signal = Float32Array.from(inputSignal);
		}
		//
		//////////

		//////////
		// Chunk signal into segments according to originalSegments
		//
		var chunks:{
			label:string,
			data:Float32Array,
			targetLength?:number
		}[] = [];

		for (let i = 0; i < originalSegments.length; ++i) {
			var begin = originalSegments[i].sampleStart;
			var end = begin + originalSegments[i].sampleDur; //no -1 here!

			chunks.push({
				label: originalSegments[i].labels[0].value,
				// slice() extracts up to but *not* including end
				data: signal.slice(begin, end)
			});
		}
		//
		//////////

		//////////
		// For each chunk, find a corresponding targetSegment
		//
		var targetSegmentIndex:number = 0;

		for (let i = 0; i < chunks.length; ++i) {
			// Chunks that are labelled as breaks do not need processing
			if (chunks[i].label === '<p:>') {
				chunks[i].targetLength = chunks[i].data.length;
			} else {
				if (targetSegmentIndex >= targetSegments.length) {
					reject('Cannot execute PSOLA, cannot find target segment' +
						' corresponding to chunk ' + i + ' ' + chunks[i].label);
					return;
				} else {
					// Skip breaks indicated as <p:>
					if (targetSegments[targetSegmentIndex].labels[0].value === '<p:>') {
						++targetSegmentIndex;

						if (targetSegmentIndex >= targetSegments.length) {
							reject('Cannot execute PSOLA, cannot find target segment' +
								' corresponding to chunk ' + i + ' ' + chunks[i].label);
							return;
						}
					}

					// Use length of the next target segment, making sure that
					// the labels match
					if (targetSegments[targetSegmentIndex].labels[0].value !== chunks[i].label) {
						reject('Cannot execute PSOLA, found deviant labels in' +
							' source chunk vs. target segment:' + chunks[i].label +
							' vs. ' + targetSegments[targetSegmentIndex].labels[0].value);
						return;
					} else {
						chunks[i].targetLength = targetSegments[targetSegmentIndex].sampleDur;
						++targetSegmentIndex;
					}
				}
			}
		}
		//
		//////////

		//////////
		// Process chunks
		//
		// First run them through a pitch tracker and then feed them into PSOLA
		//
		var promises:Promise<Float32Array>[] = [];

		for (let i = 0; i < chunks.length; ++i) {
			//console.debug('Chunk', i, chunks[i].label, chunks[i].data.length, chunks[i].targetLength);

			if (chunks[i].targetLength === chunks[i].data.length) {
				promises.push(Promise.resolve(chunks[i].data));
			} else {
				let stretchFactor:number = chunks[i].targetLength / chunks[i].data.length;

				promises.push(
					asspKsvF0(chunks[i].data, sampleRate)
						.then((pitchContour:number[]) => {
							//console.debug('chunk', i, 'stretchFactor', stretchFactor);
							return Float32Array.from(psolaAdjustSignalDuration(
								chunks[i].data,
								sampleRate,
								stretchFactor,
								pitchContour,
								wrasspPitchContourSampleRate
							));
						})
						.catch((reason) => {
							return Promise.reject({
								message: 'Could not find pitch contour',
								action: 'psolaAdjustSegmentDurations',
								previousError: reason
							});
						})
				);
			}
		}
		//
		//////////

		//////////
		// Concatenate the manipulated chunks, once all asynchronous
		// operations have been completed
		//
		Promise.all(promises)
			.then((manipulatedSignals:Float32Array[]) => {
				var signalChain:Float32Array;
				var chainLength:number = 0;

				// First find out the length of the complete chain
				for (let i = 0; i < manipulatedSignals.length; ++i) {
					chainLength += manipulatedSignals[i].length;
				}

				// Now, do the actual concatenation
				signalChain = new Float32Array(chainLength);
				var offset:number = 0;
				for (let i = 0; i < manipulatedSignals.length; ++i) {
					//console.debug('manip signal', i, manipulatedSignals[i].length, rms(manipulatedSignals[i]));
					signalChain.set(manipulatedSignals[i], offset);
					offset += manipulatedSignals[i].length;
				}

				resolve(arrayToAudioBuffer(signalChain, sampleRate));
			})
			.catch((reason) => {
				reject({
					message: 'Could not manipulate all chunks',
					action: 'psolaAdjustSegmentDurations',
					previousError: reason
				});
			});
	});
}
