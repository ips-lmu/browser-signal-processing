// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>
// (c) Georg Raess <graess@phonetik.uni-muenchen.de>
// (c) Raphael Winkelmann <raphael@phonetik.uni-muenchen.de>

import {TextgridWorker} from "./textgrid-worker.class";

export class TextgridService {
	// Function pointers to resolve/reject the current promise
	private resolve;
	private reject;

	private worker;

	constructor() {
		// Create worker and register event handler
		this.worker = new TextgridWorker();
		this.worker.says((e) => {
			if (e.status.type === 'SUCCESS') {
				this.resolve(e.data);
			} else {
				this.reject({
					message: 'Error in Textgrid worker',
					action: 'textgridService',
					previousError: e
				});
			}
		});
	}

	/**
	 * parse level data to Textgrid File
	 * @param level data
	 * @returns promise
	 */
	public asyncToTextGrid(levels, sampleRate, bufferLength) {
		return new Promise((resolve, reject) => {
			this.resolve = resolve;
			this.reject = reject;
			
			this.worker.tell({
				'cmd': 'toTextGrid',
				'levels': levels,
				'sampleRate': sampleRate,
				'buffLength': bufferLength
			});
		});
	}


	/**
	 * parse array of ssff file using webworker
	 * @param array of ssff files encoded as base64 stings
	 * @returns promise
	 */
	public asyncParseTextGrid(textGrid, sampleRate, annotates, name) {
		return new Promise((resolve, reject) => {
			this.resolve = resolve;
			this.reject = reject;

			this.worker.tell({
				'cmd': 'parseTG',
				'textGrid': textGrid,
				'sampleRate': sampleRate,
				'annotates': annotates,
				'name': name
			});
		});
	}
}
