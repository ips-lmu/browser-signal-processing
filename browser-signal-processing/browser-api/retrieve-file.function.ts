// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

/**
 * Use asynchronous XMLHttpRequest to retrieve a remote file. The file is
 * returned as an ArrayBuffer object.
 * @param url Remote URL of the file to retrieve.
 * @returns A promise resolving to the requested ArrayBuffer.
 */
export function retrieveFile(url:string):Promise<ArrayBuffer> {
	return new Promise<ArrayBuffer>((resolve, reject) => {
		var request:XMLHttpRequest = new XMLHttpRequest();

		//request.timeout = 4000; // @todo define? default is 0ms
		request.responseType = 'arraybuffer';
		request.open('GET', url);

		request.addEventListener('load', (event) => {
			if (request.response === null) {
				reject({
					message: 'Request repsonse is null',
					action: 'retrieveFile',
					previousError: event
				});
			} else if (request.status < 200 || request.status >= 300) {
				reject({
					message: 'HTTP error: ' + request.status + '/' + request.statusText,
					action: 'retrieveFile',
					previousError: event
				});
			} else {
				resolve(request.response);
			}
		});

		request.addEventListener('error', (event) => {
			reject({
				message: 'Unknown error - server might be down or unreachable',
				action: 'retrieveFile',
				previousError: event
			});
		});

		request.addEventListener('timeout', (event) => {
			reject({
				message: 'Timeout',
				action: 'retrieveFile',
				previousError: event
			});
		});

		request.send();
	});
}
