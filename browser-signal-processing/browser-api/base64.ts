// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>
// (c) Raphael Winkelmann <raphael@phonetik.uni-muenchen.de>

/**
 * Decode a base64-encoded string to an ArrayBuffer.
 *
 * @param stringBase64 A base64-encoded string.
 * @returns An ArrayBuffer object containing the decoded bytes.
 */
export function base64ToArrayBuffer (stringBase64:string):ArrayBuffer {
	var binaryString = window.atob(stringBase64);
	var len = binaryString.length;
	var bytes = new Uint8Array(len);
	for (var i = 0; i < len; i++) {
		var ascii = binaryString.charCodeAt(i);
		bytes[i] = ascii;
	}
	return bytes.buffer;
}

/**
 * Encode an ArrayBuffer as base64 string.
 *
 * @param buffer An ArrayBuffer object containing anything to be encoded.
 * @returns A base64-encoded version of `buffer`.
 */
export function arrayBufferToBase64 (buffer:ArrayBuffer):string {
	var binary = '';
	var bytes = new Uint8Array(buffer);
	var len = bytes.byteLength;
	for (var i = 0; i < len; i++) {
		binary += String.fromCharCode(bytes[i]);
	}
	return window.btoa(binary);
}
