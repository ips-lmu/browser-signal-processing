// (c) 2015-2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {estimateFinalSilence} from "../advanced/estimate-final-silence.function";
import {samplesToMilliseconds} from "../basic/conversion-functions";

/**
 * The AudioRecorder class provides an easy way to make 1-channel audio
 * recordings.
 *
 * Create a new instance of the class and call record(). That will return a
 * promise resolving to an AudioBuffer once the recording has been stopped.
 *
 * Per default, the recording will be stopped when silence is detected. Set
 * silenceDetectionDuration to determine how long the microphone has to
 * remain silent. Set it to 0 to disable silence detection.
 *
 * The recording can be stopped by calling stop(). This has to be done when
 * silence detection is turned off.
 */
export class AudioRecorder {
	// Configure recording behaviour
	private _sampleRate:number = 44100;
	private _silenceDetectionDuration:number = 500;
	private _bufferSize:number = 4096;


	// Recording state
	private recordedSignal = new Float32Array(0);
	private silenceDetectionKnownResults = [];
	private hardwareAudioContext:AudioContext = null;
	private microphone:AudioNode = null;
	private scriptProcessor:ScriptProcessorNode = null;


	// Functions to reject or resolve the promise returned by record()
	private resolve:Function;
	private reject:Function;


	/**
	 * Start recording and return a promise that is resolved when the
	 * recording has stopped.
	 *
	 * @returns A promise that resolves to the finished AudioBuffer.
	 */
	public record():Promise<AudioBuffer> {
		try {
			if (this.resolve !== undefined) {
				return Promise.reject<AudioBuffer>({
					message: 'Could not start recording',
					action: 'AudioRecorder.record',
					previousError: 'This AudioRecorder object has been used before'
				});
			}

			return new Promise<AudioBuffer>((resolve, reject) => {
				this.resolve = resolve;
				this.reject = reject;

				this.init()
					.catch((reason) => {
						reject({
							message: 'Could not start recording',
							action: 'AudioRecorder.record',
							previousError: reason
						});
					});
			});
		} catch (error) {
			return Promise.reject<AudioBuffer>({
				message: 'Could not start recording',
				action: 'AudioRecorder.record',
				previousError: error
			});
		}
	}

	/**
	 * Explicitly stop the recording. Returns nothing.
	 */
	public stop() {
		try {
			if (this.resolve === undefined) {
				return {
					message: 'Could not stop recording',
					action: 'AudioRecorder.stop',
					previousError: 'This AudioRecorder object has not been started'
				};
			}

			// Stop recording
			this.hardwareAudioContext.suspend();

			if (this.recordedSignal.length === 0) {
				this.reject({
					message: 'Tried to make empty recording',
					action: 'AudioRecorder.stop',
					previousError: 'Tried to stop recording during first' +
					' window (' + samplesToMilliseconds(this.bufferSize, this.sampleRate) + ' milliseconds)'
				});
			} else {
				var buffer = this.hardwareAudioContext.createBuffer(1, this.recordedSignal.length, this.sampleRate);
				buffer.copyToChannel(this.recordedSignal, 0);
				// Resolve promise
				this.resolve(buffer);
			}

			this.hardwareAudioContext.close();
		} catch (error) {
			this.reject({
				message: 'Error while recording',
				action: 'AudioRecorder.stop',
				previousError: error
			});
		} finally {
			// Clean
			this.microphone = null;
			this.scriptProcessor = null;
			this.recordedSignal = null;
			this.hardwareAudioContext = null;
			this.resolve = undefined;
			this.reject = undefined;
			this.silenceDetectionKnownResults = [];
		}
	}

	/**
	 * Handles AudioProcessingEvents, appends the last recorded buffer
	 * to the recordedSignal
	 *
	 * @param e [AudioProcessingEvent]
	 */
	private processAudio(e:AudioProcessingEvent) {
		try {
			// Save the buffer
			var channelData = new Float32Array(this.bufferSize);
			e.inputBuffer.copyFromChannel(channelData, 0, 0);

			var completeSignal;
			completeSignal = new Float32Array(this.recordedSignal.length + this.bufferSize);
			completeSignal.set(this.recordedSignal, 0);
			completeSignal.set(channelData, this.recordedSignal.length);

			this.recordedSignal = completeSignal;

			if (this.silenceDetectionDuration > 0) {
				// Decide whether to stop due to silence
				var durationInMS = this.silenceDetectionDuration;
				var sampleDurationInMS = 1 / this.sampleRate * 1000;
				var durationInSamples = durationInMS / sampleDurationInMS;
				
				var silenceObject = estimateFinalSilence(this.recordedSignal, durationInSamples, null, null, this.silenceDetectionKnownResults);
				this.silenceDetectionKnownResults = silenceObject.windowInfo;

				if (silenceObject.finalSilence) {
					this.stop();
				}
			}
		} catch (error) {
			this.hardwareAudioContext = null;
			this.scriptProcessor = null;
			this.microphone = null;
			this.recordedSignal = null;

			/**
			 * @todo Problem with async processing - think through all the
			 * consequences.
			 *
			 * As soon as stop() is called, processAudio() should no longer
			 * do anything. stop() does (indirectly) unregister the event
			 * handler processAudio. But if processAudio() happens to be
			 * called while stop() is running, the two of them will fight
			 * over accessing member variables.
			 *
			 * What consequences does this have on data integrity and
			 * possible runtime errors?
			 *
			 * I noticed this because a specific runtime error occurred
			 * fairly often (about 5-10% of all times when this class was
			 * used): In the try block above, accessing
			 * this.recordedSignal.length threw an exception because
			 * this.recordedSignal had been nulled by stop(). The catch
			 * block in turn threw another exception by calling this.reject
			 * (below), which had been nulled as well.
			 *
			 */

			try {
				this.reject({
					message: 'Error while recording',
					action: 'AudioRecorder.processAudio',
					previousError: error
				});
			} catch (error) {
				// This exception does not need handling, because it must
				// have been caused by this.reject not being a function any
				// more. This is only the case when our main promise has
				// been resolved anyway.
			}
		}
	}

	/**
	 * private method
	 *
	 * Connects the microphone AudioNode to our AudioContext, once it
	 * has become ready.
	 *
	 * @param stream , //@todo add type
	 */
	private connectStream(stream):Promise<void> {
		return new Promise<void>((resolve, reject) => {
			try {
				// Create an AudioNode from the stream
				this.microphone = this.hardwareAudioContext.createMediaStreamSource(stream);

				if (this.microphone === null) {
					reject({
						message: 'Could not activate microphone',
						action: 'AudioRecorderService.connectStream',
						previousError: 'createMediaStreamSource() returned null'
					});
					return;
				}

				this.microphone.connect(this.scriptProcessor);

				resolve(undefined);
			} catch (error) {
				this.microphone = null;
				reject({
					message: 'Could not activate microphone',
					action: 'AudioRecorderService.connectStream',
					previousError: error
				});
			}
		});
	}

	/**
	 * Set up hardwareAudioContext and scriptProcessor.
	 *
	 * @returns A promise that is resolved once all has been set up or
	 *           rejected if something fails along the way.
	 */
	private init():Promise<void> {
		return new Promise<void>((resolve, reject) => {
			try {
				if (!AudioContext) {
					reject({
						message: 'Could not start audio recorder',
						action: 'AudioRecorderService.init',
						previousError: 'AudioContext is not supported in your browser'
					});
					return;
				}

				this.hardwareAudioContext = new AudioContext();

				if (this.hardwareAudioContext === null) {
					reject({
						message: 'Could not start audio recorder',
						action: 'AudioRecorderService.init',
						previousError: 'new() AudioContext returned null'
					});
					return;
				}

				// Set up ScriptProcessor
				this.scriptProcessor = this.hardwareAudioContext.createScriptProcessor(this.bufferSize, 1, 1);
				this.scriptProcessor.addEventListener("audioprocess", (buffer) => {
					this.processAudio(buffer);
				});
				this.scriptProcessor.connect(this.hardwareAudioContext.destination);

				// Set up GetUserMedia
				if (!(<any>navigator).webkitGetUserMedia) {
					reject({
						message: 'Could not start audio recorder',
						action: 'AudioRecorderService.init',
						previousError: 'webkitGetUserMedia is not supported  in your browser'
					});
					return;
				}


				(<any>navigator).webkitGetUserMedia(
					// What kind of media to record
					{audio: true},

					// Success callback
					(stream) => {
						this.connectStream(stream)
							.then(() => {
								resolve(undefined);
							})
							.catch((reason) => {
								this.hardwareAudioContext = null;
								this.scriptProcessor = null;
								reject({
									message: 'Could not start audio recorder',
									action: 'AudioRecorderService.init',
									previousError: reason
								})
							});
					},

					// Error callback
					(error) => {
						this.hardwareAudioContext = null;
						this.scriptProcessor = null;
						reject({
							message: 'Could not start audio recorder',
							action: 'AudioRecorderService.init',
							previousError: error
						});
					}
				);
			} catch (error) {
				this.hardwareAudioContext = null;
				this.scriptProcessor = null;

				reject({
					message: 'Could not start audio recorder',
					action: 'AudioRecorderService.init',
					previousError: error
				});
				return;
			}
		});
	}

	get bufferSize():number {
		return this._bufferSize;
	}

	set bufferSize(value:number) {
		this._bufferSize = value;
	}
	get silenceDetectionDuration():number {
		return this._silenceDetectionDuration;
	}

	set silenceDetectionDuration(value:number) {
		this._silenceDetectionDuration = value;
	}
	get sampleRate() {
		return this._sampleRate;
	}

	set sampleRate(value) {
		this._sampleRate = value;
	}
}
