// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

/**
 * Decode the contents of an ArrayBuffer object and save as a string.
 * @param buf The object to decode
 * @returns The decoded string.
 */
export function arrayBufferToString (buf: ArrayBuffer):string {
	var dataView = new DataView(buf);
	var decoder = new TextDecoder();
	return decoder.decode(dataView);
}

/**
 * Encode a number[] or Float32Array as an AudioBuffer.
 * @param array An array containing a one-channel audio signal.
 * @param sampleRate The sample rate of the signal in `array`.
 * @returns An AudioBuffer object containing a copy of the signal in `array`.
 */
export function arrayToAudioBuffer(array:number[]|Float32Array, sampleRate:number):AudioBuffer {
	var context = new OfflineAudioContext(1, 4096, sampleRate);
	var buffer = context.createBuffer(
		1,
		array.length,
		sampleRate
	);
	if (array instanceof Array) {
		buffer.copyToChannel(new Float32Array(array), 0);
	} else {
		buffer.copyToChannel(array, 0);
	}
	return buffer;
}

/**
 * Decode an audio file to an AudioBuffer object.
 *
 * Supported input formats are determined by the browser. The WebAudio API
 * re-samples the signal to the given sample rate. If resampling is
 * undesired, the source sample rate must be known beforehand.
 *
 * @param file An object containing the encoded input file.
 * @param sampleRate The sample rate of the target AudioBuffer object.
 * @returns A promise resolving to the requested AudioBuffer.
 */
export function decodeAudioFile(file:ArrayBuffer, sampleRate:number):Promise<AudioBuffer> {
	return new Promise<AudioBuffer>((resolve, reject) => {
		var context = new OfflineAudioContext(1, 4096, sampleRate);

		context.decodeAudioData(file)
			.then((value:AudioBuffer) => {
				resolve(value);
			})

			.catch((reason) => {
				reject({
					message: 'Could not decode audio file',
					action: 'decodeAudioFile',
					previousError: reason
				});
			});
	});
}
