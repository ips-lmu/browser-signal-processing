// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {windowHann} from "../basic/window-functions";
import {secondsToSamples, samplesToSeconds} from "../basic/conversion-functions";

/**
 * Estimate positions of pitch marks from a discrete pitch contour.
 *
 * In a discrete pitch contour, we have a number of samples indicating the
 * pitch in hertz at a particular point in time. The samples are equally spaced,
 * i.e. the time between any two samples is fixed.
 *
 * Pitch marks are the positions where pitch periods begin (where they
 * "begin" is a simplification because, technically, they do not have a
 * beginning). The result will therefore be an array. The array's length
 * equals the number of pitch periods in the signal corresponding to the
 * given pitchContour.
 *
 * @param pitchContour A discrete signal containing the pitch contour of an
 *                      audio signal.
 * @param sampleRate The sample rate of `pitchContour`.
 * @returns An array of pitch marks.
 */
export function pitchContourToPitchMarkSet(pitchContour:number[], sampleRate:number):number[] {
	/**
	 * In voiceless portions, pitch marks cannot be defined in terms of
	 * pitch periods. They are therefore set at a constant interval, which
	 * can be defined here, in seconds.
	 *
	 * @type {number}
	 */
	var voicelessPitchMarkDistance:number = 0.005;

	/**
	 * While processing pitchContour, we always estimate to be within a
	 * certain pitch period. startOfCurrentPitchPeriod holds, measured
	 * in seconds, where the currently processed pitch period starts.
	 *
	 * If startOfCurrentPitchPeriod is -1, we are currently processing a
	 * voiceless portion of the pitch contour.
	 *
	 * @type {number}
	 */
	var startOfCurrentPitchPeriod:number = -1;

	// Initialise the result
	var pitchMarks:number[] = [];

	//
	// Process pitchContour
	for (let i = 0; i < pitchContour.length; ++i) {
		if (pitchContour[i] === 0) {
			// The current sample is in a voiceless portion
			startOfCurrentPitchPeriod = -1;

			// See whether the time between the last pitch mark and the
			// current position exceeds a certain threshold and if so, add
			// another pitch mark. This is done regardless of whether the
			// last pitch mark was in a voiced or voicesless portion.
			if (pitchMarks.length === 0) {
				var time = samplesToSeconds(i, sampleRate);
			} else {
				var time = samplesToSeconds(i, sampleRate) - pitchMarks[pitchMarks.length - 1];
			}

			if (time > voicelessPitchMarkDistance) {
				pitchMarks.push(samplesToSeconds(i, sampleRate));
			}

			// console.debug(
			// 	'Pitch contour vcl at sample', i,
			// 	'at time', samplesToSeconds(i, sampleRate),
			// 	'. VCL window size:', time
			// );
		} else {
			// The current sample is in a voiced portion

			if (startOfCurrentPitchPeriod === -1) {
				// The last sample was in a voiceless portion, so the
				// voicing just set in here. We add a pitch mark for this
				// position.
				startOfCurrentPitchPeriod = samplesToSeconds(i, sampleRate);
				pitchMarks.push(samplesToSeconds(i, sampleRate));
				//console.debug('Detected start of voiced period at sample',
				// i, 'at time', samplesToSeconds(i, sampleRate));

				// We also add another pitch mark based on the current pitch.
				// This pitch mark might be corrected, though, when the pitch
				// values that lie between here and that pitch mark deviate
				// from the current pitch value.
				pitchMarks.push(samplesToSeconds(i, sampleRate) + 1 / pitchContour[i]);
			} else {
				//////////
				// The last sample was already in a voiced portion, so we
				// need to estimate whether we are still within the same
				// pitch period. If we are, we re-calculate the  next pitch mark
				// (which has already been added and marks the end of the
				// current pitch period).
				//

				// To inform the estimation, we calculate the average pitch
				// from the beginning of the current pitch period to the
				// current position.
				var startSample = secondsToSamples(startOfCurrentPitchPeriod, sampleRate);

				var tally = 0;
				for (let j = startSample; j <= i; ++j) {
					tally += pitchContour[j];
				}
				var average = tally / (i - startSample + 1);

				/*
				 console.debug(
				 'Pitch mark was added at ', pitchMarks[pitchMarks.length-1],
				 'but is now moved to', startOfCurrentPitchPeriod + 1 / average
				 );
				 */

				// Adjust the next pitch mark to reflect not only the first
				// pitch value that lies within the current pitch period,
				// but all of them.
				pitchMarks.pop();
				pitchMarks.push(startOfCurrentPitchPeriod + 1 / average);

				//
				//////////

				//////////
				// The next pitch mark has been adjusted. Now we can check
				// whether pitchContour[i] lies beyond that boundary. If so,
				// we have reached a new pitch period and have to reset
				// variables accordingly.
				//

				if (samplesToSeconds(i, sampleRate) >= pitchMarks[pitchMarks.length - 1]) {
					startOfCurrentPitchPeriod = pitchMarks[pitchMarks.length - 1];

					// Again, we guess the next pitch mark, but it might be
					// corrected during the upcoming iterations.
					pitchMarks.push(samplesToSeconds(i, sampleRate) + 1 / pitchContour[i]);
				}

				//
				//////////
			}
		}
	}

	/*
	 console.debug('Pitch contour', pitchContour);
	 console.debug('Pitch marks', pitchMarks);
	 */
	return pitchMarks;
}

/**
 * Perform the analysis step of PSOLA.
 *
 * This basically means to calculate pitch marks for the input signal.
 *
 * Currently depends on a discrete pitch contour to be passed in, rather than
 * the input signal itself.
 *
 * @param sampleRate The sample rate of the input signal.
 * @param pitchContour The pitch contour associated with {signal}.
 * @param pitchContourSampleRate The sample rate of {pitchContour}.
 * @returns An array of pitch marks.
 */
export function psolaAnalysis(sampleRate:number,
                              pitchContour:number[],
                              pitchContourSampleRate:number):number[] {
	// Transform pitch contour into a set of pitch marks whose units are
	// seconds
	var pitchMarks:number[] =
		pitchContourToPitchMarkSet(pitchContour, pitchContourSampleRate);

	// Transform seconds into samples
	for (let i = 0; i < pitchMarks.length; ++i) {
		pitchMarks[i] = secondsToSamples(pitchMarks[i], sampleRate);
	}

	return pitchMarks;
}

/**
 * Perform the synthesis step of PSOLA.
 *
 * Find a mapping from synthesis pitch marks to analysis pitch marks (each
 * SPM must be mapped to exactly one APM; APMs may or not be a mapping
 * target and may also be the mapping target for multiple SPMs).
 *
 * @param signal
 * @param analysisPitchMarks
 * @param synthesisPitchMarks
 * @returns
 */
export function psolaSynthesis(signal:number[]|Float32Array,
                               analysisPitchMarks:number[],
                               synthesisPitchMarks:number[],
                               stretchFactor:number,
                               windowSizeFactor:number = 2):number[] {
	if (analysisPitchMarks.length === 0) {
		// @todo Indicate error somehow
		return;
	}

	//////////
	// Map synthesis pitch marks to analysis pitch marks
	//
	// This array will contain one element for each synthesis pitch mark.
	// Each element will be the index of an analysis pitch mark.
	var pitchMarkMapping:number[] = [];

	for (let i = 0; i < synthesisPitchMarks.length; ++i) {
		// Find the analysis pitch mark nearest to synthesisPitchMark[i],
		// but compensate for stretchFactor.
		var target:number = synthesisPitchMarks[i] / stretchFactor;

		var nearestAPM:number = 0;
		var nearestDistance:number = Math.abs(target - analysisPitchMarks[nearestAPM]);

		for (let j = 1; j < analysisPitchMarks.length; ++j) {
			if (Math.abs(target - analysisPitchMarks[j]) < nearestDistance) {
				nearestAPM = j;
				nearestDistance = Math.abs(target - analysisPitchMarks[nearestAPM]);
			} else {
				break;
			}
		}

		pitchMarkMapping.push(nearestAPM);
	}

	//////////
	// Cut windows from signal
	//
	var targetWindows:number[][] = [];
	for (let i = 0; i < pitchMarkMapping.length; ++i) {
		var localPeriodDuration:number;
		if (pitchMarkMapping[i] === 0) {
			localPeriodDuration = analysisPitchMarks[pitchMarkMapping[0]];
		} else {
			localPeriodDuration = analysisPitchMarks[pitchMarkMapping[i]] - analysisPitchMarks[pitchMarkMapping[i] - 1];
		}
		/*
		 console.debug('Cutting window from source - centered at analysis' +
		 ' pitch mark', pitchMarkMapping[i], 'which is at sample',
		 analysisPitchMarks[pitchMarkMapping[i]], 'and has a local pitch' +
		 ' period of ', localPeriodDuration);
		 */


		var windowSize:number = Math.round(localPeriodDuration * windowSizeFactor);
		var windowStart:number = Math.ceil(analysisPitchMarks[pitchMarkMapping[i]] - windowSize / 2);
		var window:number[] = [];

		for (let j = 0; j < windowSize; ++j) {
			var sourceSampleIndex:number = windowStart + j;
			var sourceSample:number;

			if (sourceSampleIndex >= 0 && sourceSampleIndex < signal.length) {
				sourceSample = signal[sourceSampleIndex];
			} else {
				sourceSample = 0;
			}

			window.push(sourceSample * windowHann(windowSize, j));
		}

		targetWindows.push(window);
	}

	//////////
	// Overlap and add
	//
	// We now have all our properly Hann'ed target windows in targetWindows[]
	// and their center point within the target signal is stored in
	// synthesisPitchMarks[].
	//

	var synthesisSignal:number[] = [];
	var synthesisLength:number = Math.floor(signal.length * stretchFactor);
	for (let i = 0; i < synthesisLength; ++i) {
		synthesisSignal.push(0);
	}

	for (let i = 0; i < targetWindows.length; ++i) {
		// Define a region (start, center, end) in the synthesis signal
		// where the current window is to be placed
		var windowCenter:number = synthesisPitchMarks[i];
		var windowStart:number = synthesisPitchMarks[i] - Math.floor(targetWindows[i].length / 2);
		var windowEnd:number = windowStart + targetWindows[i].length - 1;

		// Define a portion of the window that fits in the boundaries of the
		// synthesis signal
		var firstSample:number = 0;
		var lastSample:number = targetWindows[i].length;

		if (windowStart < 0) {
			firstSample = -windowStart;
		}

		if (windowEnd >= synthesisSignal.length) {
			lastSample = synthesisSignal.length - windowStart;
		}

		// Add all samples from targetWindows[i] to synthesisSignal
		for (let j = 0; j < targetWindows[i].length; ++j) {
			var targetIndex:number = windowStart + j;

			if (targetIndex >= 0 && targetIndex < synthesisSignal.length) {
				synthesisSignal[targetIndex] += targetWindows[i][j];
			}
		}
	}

	return synthesisSignal;
}

/**
 * Use PSOLA to adjust the duration of an audio signal, retaining the original
 * pitch contour.
 *
 * @param signal The audio signal to be manipulated
 * @param stretchFactor A factor by which to multiply the original
 *                                signal's duration
 * @returns The manipulated signal
 */
export function psolaAdjustSignalDuration(signal:number[]|Float32Array,
                                          sampleRate:number,
                                          stretchFactor:number,
                                          pitchContour:number[],
                                          pitchContourSampleRate:number):number[] {
	// signal is real-valued within [-1; 1]

	// A pitch mark is the number of a sample where a pitch period "begins".
	// Contains positive integer values; values are strictly increasing
	var analysisPitchMarks:number[] = psolaAnalysis(sampleRate, pitchContour, pitchContourSampleRate);;
	//console.debug(analysisPitchMarks);

	// @todo move this loop to a unit test
	for (let i = 0; i < analysisPitchMarks.length; ++i) {
		if (analysisPitchMarks[i] < 0) {
			console.error('Analysis pitch mark is below 0', i, analysisPitchMarks[i]);
			;
		}
		if (i > 0 && (analysisPitchMarks[i] <= analysisPitchMarks[i - 1])) {
			console.error('Analysis pitch mark is left of its left neighbour', i);
		}
	}

	// Calculate period durations (first derivative of analysisPitchMarks)
	// Contains only positive integers, since analysisPitchMarks is strictly
	// increasing
	var periodDurations:number[] = [];

	periodDurations.push(analysisPitchMarks[0]);
	for (var i = 1; i < analysisPitchMarks.length; ++i) {
		periodDurations.push(analysisPitchMarks[i] - analysisPitchMarks[i - 1]);
	}

	// In order to stretch the signal by a factor of x and still retain the
	// original pitch contour, we will need x times as many periods
	var numInputPeriods:number = analysisPitchMarks.length;
	var numOutputPeriods:number = Math.round(numInputPeriods * stretchFactor);

	// Period duration is the inverse of fundamental frequency. Retaining
	// the contour of original period durations is therefore equivalent to
	// retaining the original pitch contour.
	//
	// The contour of period durations is defined by the discrete period
	// durations that we have stored already. That is, we know
	// {numInputPeriods} samples of that contour. Note though that these
	// samples are not equally spaced. We now need to re-sample the contour to
	// {numOutputPeriods} samples.
	//
	// We do this by means of linear interpolation.

	var outputPeriodDurations:number[] = [];
	for (var i = 0; i < numOutputPeriods; ++i) {
		// This index will most likely not be a natural number
		var correspondingPeriodIndex:number = i / stretchFactor;

		// Find the two points between which to interpolate
		var leftNeighbour:number = Math.floor(correspondingPeriodIndex);
		var rightNeighbour:number = Math.ceil(correspondingPeriodIndex);

		// On the right side of the contour, the right point might lie beyond
		// the contour, in which case we just use the last available point.
		// I think the left point will never lie beyond the contour, but I
		// can't prove.
		if (rightNeighbour >= periodDurations.length) {
			rightNeighbour = leftNeighbour;
		}

		// Do linear interpolation
		var leftWeight = correspondingPeriodIndex - leftNeighbour;
		var rightWeight = 1 - leftWeight;
		outputPeriodDurations.push(Math.round(
			leftWeight * periodDurations[leftNeighbour]
			+ rightWeight * periodDurations[rightNeighbour]
		));
	}

	// We now have a re-sampled first derivative. We integrate it to get the
	// re-sampled pitch mark positions (synthesisPitchMarks).
	var synthesisPitchMarks:number[] = [];
	synthesisPitchMarks.push(outputPeriodDurations[0]);
	for (var i = 1; i < outputPeriodDurations.length; ++i) {
		synthesisPitchMarks.push(outputPeriodDurations[i] + synthesisPitchMarks[i - 1]);
	}

	return psolaSynthesis(signal, analysisPitchMarks, synthesisPitchMarks, stretchFactor);
}
