// (c) 2015-2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

/**
 * @todo revise this documentation since it reflects what the function was
 * planned to be like. It still has the same purpose but acts a little
 * differently.
 *
 * @todo add types
 *
 * Estimate the point of a signal where the desired signal ends and
 * silence starts.
 *
 * The estimation depends on a number of unknown factors. This is
 * all the more true, since it is done without prior calibration.
 * The factors include:
 * - The signal-to-noise ratio (SNR), which in turn depends on
 * - The technical background noise (caused by the recording system)
 * - The actual background noise (present in the recording
 *   environment)
 * - The energy of the desired signal
 *
 * This function can give a good estimate when the actual background
 * noise is steady (it need not be very low).
 *
 * @param signal
 * @param durationThreshold (optional) [in samples] defaults to 22050
 * @param relativeEnergyThreshold (optional) [relative unit] defaults to 0.05
 * @param windowSize (optional) [in samples] defaults to 512
 * @param knownResults (optional) an array with already known
 *        information on the first part of the signal that do not
 *        need to be calculated again
 */
export function estimateFinalSilence(signal, durationThreshold, relativeEnergyThreshold, windowSize, knownResults) {
	// Set default values
	if (durationThreshold === null) {
		durationThreshold = 22050;
	}
	if (relativeEnergyThreshold === null) {
		relativeEnergyThreshold = 0.05;
	}
	if (windowSize === null) {
		windowSize = 512;
	}

	// Window the signal and for each window calculate:
	// - RMS of that window
	// - Minimal RMS encountered in that or the preceding windows
	// - Maximal RMS encountered in that or the preceding windows
	// - SNR = maximal RMS / minimal RMS
	// - Relative energy: RMS of that window / SNR

	var numberOfWindows = Math.ceil(signal.length / windowSize);

	// The processing results are stored in windowInfo
	var windowInfo;

	// If some results have already been calculated, they are passed
	// in via knownResults
	if (knownResults) {
		windowInfo = knownResults;
	} else {
		windowInfo = [];
	}

	// Iterate over all the windows that have not yet been processed

	for (var i:number = windowInfo.length; i < numberOfWindows; ++i) {
		// Initialise windowInfo object
		windowInfo.push({
			totalSquareEnergy: 0,
			rms: null,
			rmsMinimum: null,
			rmsMaximum: null,
			snr: null,
			relativeEnergy: null
		});

		// Look at every sample of this window
		for (var j = 0; j < windowSize; ++j) {
			var sampleIndex = i * windowSize + j;
			if (sampleIndex >= signal.length) {
				break;
			}
			windowInfo[i].totalSquareEnergy += signal[sampleIndex] * signal[sampleIndex];
		}

		// Calculate RMS
		windowInfo[i].rms = Math.sqrt(windowInfo[i].totalSquareEnergy / windowSize);

		// Look for minimal RMS by comparing this window's RMS and
		// the last window's rmsMinimum
		if (i === 0 || windowInfo[i - 1].rmsMinimum === null || windowInfo[i].rms < windowInfo[i - 1].rmsMinimum) {
			if (windowInfo[i].rms === 0) {
				// The rmsMinimum cannot be 0 (otherwise SNR would
				// be undefined). RMS 0 can only appear in a system
				// that is turned off, anyway. It is thus a good
				// idea to throw away such values, rather than treat
				// them as a real minimum.
				windowInfo[i].rmsMinimum = null;
			} else {
				windowInfo[i].rmsMinimum = windowInfo[i].rms;
			}
		} else {
			windowInfo[i].rmsMinimum = windowInfo[i - 1].rmsMinimum;
		}
		// Do the same for maximal RMS
		if (i === 0 || windowInfo[i].rms > windowInfo[i - 1].rmsMaximum) {
			windowInfo[i].rmsMaximum = windowInfo[i].rms;
		} else {
			windowInfo[i].rmsMaximum = windowInfo[i - 1].rmsMaximum;
		}

		// Calculate SNR
		if (windowInfo[i].rmsMinimum === 0) {
			windowInfo[i].snr = 0;
		} else {
			windowInfo[i].snr = windowInfo[i].rmsMaximum / windowInfo[i].rmsMinimum;
		}

		// Calculate relative energy
		if (windowInfo[i].rmsMaximum === windowInfo[i].rmsMinimum) {
			windowInfo[i].relativeEnergy = 1;
		} else {
			windowInfo[i].relativeEnergy = (windowInfo[i].rms - windowInfo[i].rmsMinimum) / (windowInfo[i].rmsMaximum - windowInfo[i].rmsMinimum);
		}

		//console.debug( 'Window:', i, 'RMS', windowInfo[i].rms, 'SNR:', windowInfo[i].snr, 'Relative Energy:', windowInfo[i].relativeEnergy);
	}

	// The complete signal has been analysed now.

	// Now, try to figure out whether there is silence at the end.

	// We define silence such that the relativeEnergy value stays
	// below a given threshold for at least a given duration.

	// That duration is given as a number of samples, but we can
	// only operate on the window level. In case durationThreshold
	// is not a multiple of windowSize, we use the next larger
	// multiple.
	var durationInWindows = Math.ceil(durationThreshold / windowSize);


	// We assume there is final silence until we find one window
	// that suggests otherwise
	var result = {
		finalSilence: true,
		windowInfo: windowInfo
	};

	// Look back at the last n windows
	if (windowInfo.length < durationInWindows) {
		result.finalSilence = false;
		return result;
	}

	for (var i = windowInfo.length - durationInWindows; i < windowInfo.length; ++i) {
		if (windowInfo[i].relativeEnergy > relativeEnergyThreshold) {
			result.finalSilence = false;
			return result;
		}
	}

	return result;
}
