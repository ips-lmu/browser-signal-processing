// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

/**
 * This module provides an interface to a number of RESTful web services
 * offered by the Bavarian Archive for Speech Signals (BAS). Please refer to
 * the BAS Terms of Usage  at
 * <https://clarin.phonetik.uni-muenchen.de/BASWebServices/#/help>. For
 * non-commercial, academic purposes, the services are free to use.
 *
 * The supported services are MAUS (WebMAUS General), MAUSBasic (WebMAUS
 * Basic), G2P, and Pho2Syl. Please refer to the service documentation at
 * <https://clarin.phonetik.uni-muenchen.de/BASRepository/WebServices/BAS_Webservices.cmdi.xml>.
 *
 * A very short explanation of the individual services
 *
 * MAUS (Munich Automatic Segmentation) processes a speech signal along with
 * its orthographic transcription, and produces a segmentation into words and
 * phones/phonemes.
 *
 * G2P (grapheme to phoneme) returns a canonical phonemic transcription for
 * a given orthographic input text.
 *
 * Pho2Syl (phoneme to syllable) provides syllabification of canonical or
 * spontaneous speech transcriptions.
 *
 */

export * from "./bas-services/bas-service-result";
export * from "./bas-services/maus";
export * from "./bas-services/g2p";
export * from "./bas-services/pho2syl";
