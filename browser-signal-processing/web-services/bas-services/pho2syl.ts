// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {BASServiceResult, parseBASServiceResult} from "./bas-service-result";
import {retrieveFile} from "../../browser-api/retrieve-file.function";

/**
 * Output formats provided by the Pho2Syl service.
 */
export type Pho2SylOutputFormat = 'tg' | 'bpf';
/**
 * Tiers that Pho2Syl can process.
 */
export type Pho2SylTier = 'KAN' | 'MAU' | 'PHO' | 'SAP';

/**
 * RESTfully call the Pho2Syl service, to get a syllabification of canonical
 * or spontaneous speech transcriptions.
 *
 * Please refer to the BAS Terms of Usage at
 * <https://clarin.phonetik.uni-muenchen.de/BASWebServices/#/help> and to the
 * service documentation at
 * <https://clarin.phonetik.uni-muenchen.de/BASRepository/WebServices/BAS_Webservices.cmdi.xml>.
 *
 * @param transcription The transcription to convert, in BPF format. Either as a
 *                       string or as an ArrayBuffer or Blob object containing
 *                       a BPF file.
 * @param language Language of `transcription`. Can be a code like 'deu', see
 *                  service documentation for a list of accepted languages.
 * @param tier Name of tier to be syllabified.
 * @param outputFormat Output format to request from the service. See
 *                      service documentation for a list of available options.
 * @param inputFileName When uploaded to the service (as upload files),
 *                       `transcription` gets a file name composed of
 *                       `inputFileName` and '.par'.
 * @param options Additional options to be passed to the service. See service
 *                 documentation for a list of accepted options.
 * @param serviceURL The URL at which the REST API is located.
 * @returns A promise resolving to the requested phonemic transcription.
 */
export function pho2syl(transcription:string|ArrayBuffer|Blob,
                        language:string,
                        outputFormat:Pho2SylOutputFormat,
                        tier: Pho2SylTier = 'MAU',
                        inputFileName:string = 'browser-signal-processing',
                        options:Pho2SylOptions = {},
                        serviceURL:string = 'https://clarin.phonetik.uni-muenchen.de/BASWebServices/services/runPho2Syl'):Promise <ArrayBuffer> {
	return new Promise((resolve, reject) => {
		//////////
		// Generate input file Blob
		//
		var inputFile:Blob;

		if (typeof transcription === 'string' || transcription instanceof ArrayBuffer) {
			inputFile = new Blob([transcription], {type: 'text/plain'});
		} else {
			inputFile = transcription;
		}
		//
		//////////

		//////////
		// Construct request
		//
		var formData = new FormData();

		formData.append('i', inputFile, inputFileName + '.par');
		formData.append('lng', language);
		formData.append('tier', tier);
		formData.append('oform', outputFormat);

		for (var p in options) {
			if (options.hasOwnProperty(p)) {
				formData.append(p, options[p]);
			}
		}

		var request = new XMLHttpRequest();
		//
		//////////

		//////////
		// Event handlers
		//
		request.addEventListener('load', (event) => {
			var result:BASServiceResult = parseBASServiceResult(request.responseXML);

			if (result.success === true) {
				retrieveFile(result.downloadLink)
					.then((file) => {
						resolve(file);
					})
					.catch((reason) => {
						reject({
							message: 'Could not download result',
							action: 'pho2syl',
							previousError: reason
						});
					});
			} else {
				// @todo use standardised error message as rejection reason
				reject({
					message: 'Web service failed',
					action: 'pho2syl',
					previousError: result
				});
			}
		});

		request.addEventListener('error', (event) => {
			reject({
				message: 'Unknown error - BAS server might be down or unreachable',
				action: 'pho2syl',
				previousError: event
			});
		});

		request.addEventListener('timeout', (event) => {
			reject({
				message: 'Timeout',
				action: 'pho2syl',
				previousError: event
			});
		});
		//
		//////////

		//////////
		// Send request
		//
		request.open('POST', serviceURL);
		request.overrideMimeType('text/xml');
		request.send(formData);
	});
}

/**
 * Advanced options that can be passed to the Pho2Syl service. See service
 * documenatiton for details.
 */
export interface Pho2SylOptions {
	wsync?: 'yes' | 'no';
	rate?: number;
}
