// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {BASServiceResult, parseBASServiceResult} from "./bas-service-result";
import {retrieveFile} from "../../browser-api/retrieve-file.function";

/**
 * Input formats accepted by the G2P service.
 */
export type G2PInputFormat = 'txt' | 'bpf' | 'list' | 'tcf' | 'tg';

/**
 * Output formats provided by the G2P service.
 */
export type G2POutputFormat = 'txt' | 'tab' | 'exttab' | 'lex' | 'extlex' | 'bpf' | 'bpfs' | 'tcf' | 'exttcf' | 'tg' | 'exttg';

/**
 * RESTfully call the G2P service, to get a canonical phonemic transcription
 * based on an orthographic transcription.
 *
 * Please refer to the BAS Terms of Usage at
 * <https://clarin.phonetik.uni-muenchen.de/BASWebServices/#/help> and to the
 * service documentation at
 * <https://clarin.phonetik.uni-muenchen.de/BASRepository/WebServices/BAS_Webservices.cmdi.xml>.
 *
 * @param orthography The transcription to convert. Either as string or as
 *                     an ArrayBuffer or Blob object containing a plain text
 *                     file.
 * @param language Language of `orthography`. Can be a code like 'deu', see
 *                  service documentation for a list of accepted languages.
 * @param outputFormat Output format to request from the service. See
 *                      service documentation for a list of available options.
 * @param inputFormat Format of `orthography`. See service documentation for
 *                     a list of available options.
 * @param inputFileName When uploaded to the service (as upload files),
 *                       `orthography` gets a file name composed of
 *                       `inputFileName` and an extension appropriate for
 *                       `inputFormat`.
 * @param options Additional options to be passed to the service. See service
 *                 documentation for a list of accepted options.
 * @param serviceURL The URL at which the REST API is located.
 * @returns A promise resolving to the requested phonemic transcription.
 */
export function g2p(orthography:string|ArrayBuffer|Blob,
                    language:string,
                    outputFormat:G2POutputFormat,
                    inputFormat:G2PInputFormat = 'txt',
                    inputFileName:string = 'browser-signal-processing',
                    options:Object = {},
                    serviceURL:string = 'https://clarin.phonetik.uni-muenchen.de/BASWebServices/services/runG2P'):Promise <ArrayBuffer> {
	return new Promise((resolve, reject) => {
		//////////
		// Generate input file Blob
		//
		var inputFile:Blob;

		if (typeof orthography === 'string') {
			inputFormat = 'txt';
			inputFile = new Blob([orthography], {type: 'text/plain'});
		} else if (orthography instanceof ArrayBuffer) {
			inputFile = new Blob([orthography], {type: 'text/plain'});
		} else {
			inputFile = orthography;
		}
		//
		//////////

		//////////
		// Generate input file name
		switch (inputFormat) {
			case 'tg':
				inputFileName += '.TextGrid';
				break;
			case 'bpf':
				inputFileName += '.par';
				break;
			default:
				inputFileName += '.' + inputFormat;
		}
		//
		//////////

		//////////
		// Construct request
		//
		var formData = new FormData();

		formData.append('i', inputFile, inputFileName);
		formData.append('lng', language);
		formData.append('iform', inputFormat);
		formData.append('oform', outputFormat);

		for (var p in options) {
			if (options.hasOwnProperty(p)) {
				formData.append(p, options[p]);
			}
		}

		var request = new XMLHttpRequest();
		//
		//////////

		//////////
		// Event handlers
		//
		request.addEventListener('load', (event) => {
			var result:BASServiceResult = parseBASServiceResult(request.responseXML);

			if (result.success === true) {
				retrieveFile(result.downloadLink)
					.then((file) => {
						resolve(file);
					})
					.catch((reason) => {
						reject({
							message: 'Could not download result',
							action: 'g2p',
							previousError: reason
						});
					});
			} else {
				// @todo use standardised error message as rejection reason
				reject({
					message: 'Web service failed',
					action: 'g2p',
					previousError: result
				});
			}
		});

		request.addEventListener('error', (event) => {
			reject({
				message: 'Unknown error - BAS server might be down or unreachable',
				action: 'g2p',
				previousError: event
			});
		});

		request.addEventListener('timeout', (event) => {
			reject({
				message: 'Timeout',
				action: 'g2p',
				previousError: event
			});
		});
		//
		//////////

		//////////
		// Send request
		//
		request.open('POST', serviceURL);
		request.overrideMimeType('text/xml');
		request.send(formData);
	});
}
