// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {BASServiceResult, parseBASServiceResult} from "./bas-service-result";
import {audioBufferToWav} from "../../formats/wave/audio-buffer-to-wav.function";
import {retrieveFile} from "../../browser-api/retrieve-file.function";

/**
 * RESTfully call the WebMAUS Basic service, to automatically segment a
 * signal into words and phonemes based on an orthographic transcription.
 *
 * Please refer to the BAS Terms of Usage at
 * <https://clarin.phonetik.uni-muenchen.de/BASWebServices/#/help> and to the
 * service documentation at
 * <https://clarin.phonetik.uni-muenchen.de/BASRepository/WebServices/BAS_Webservices.cmdi.xml>.
 *
 * @param signal An audio signal to segment. Either as an AudioBuffer object
 *                or as an ArrayBuffer or Blob object containing a WAVE,
 *                NIST/SPHERE, or ALAW file.
 * @param text Transcription to accompany `signal`. Either as string or as
 *              an ArrayBuffer or Blob object containing a plain text file.
 * @param language Language of `signal` and `text`. Can be a code like
 *                  'deu', see service documentation for a list of accepted
 *                  languages.
 * @param inputFileName When uploaded to the service (as upload files),
 *                       `text` and `signal` get a file name composed of
 *                       `inputFileName` and '.txt' or '.wav', respectively.
 * @param serviceURL The URL at which the REST API is located.
 * @returns A promise resolving to the requested segmentation.
 */
export function mausBasic(signal:AudioBuffer | ArrayBuffer | Blob,
                          text:string | ArrayBuffer | Blob,
                          language:string,
                          inputFileName:string = 'browser-signal-processing',
                          serviceURL:string = 'https://clarin.phonetik.uni-muenchen.de/BASWebServices/services/runMAUSBasic'):Promise<ArrayBuffer> {
	return new Promise((resolve, reject) => {
		//////////
		// Generate input files
		//
		var signalFile:Blob;
		var textFile:Blob;

		if (signal instanceof AudioBuffer) {
			var wave:ArrayBuffer = audioBufferToWav(signal);
			signalFile = new Blob([wave], {type: 'audio/x-wav'});
		} else if (signal instanceof ArrayBuffer) {
			signalFile = new Blob([signal], {type: 'audio/x-wav'})
		} else {
			signalFile = signal;
		}

		if (typeof text === 'string' || text instanceof ArrayBuffer) {
			textFile = new Blob([text], {type: 'text/plain'});
		} else {
			textFile = text;
		}
		//
		//////////

		//////////
		// Construct request
		//
		var formData = new FormData();

		formData.append('TEXT', textFile, inputFileName + '.txt');
		formData.append('LANGUAGE', language);
		formData.append('SIGNAL', signalFile, inputFileName + '.wav');

		var request = new XMLHttpRequest();
		//
		//////////

		//////////
		// Event handlers
		//
		request.addEventListener('load', (event) => {
			var result:BASServiceResult = parseBASServiceResult(request.responseXML);

			if (result.success === true) {
				retrieveFile(result.downloadLink)
					.then((file) => {
						resolve(file);
					})
					.catch((reason) => {
						reject({
							message: 'Could not download result',
							action: 'mausBasic',
							previousError: reason
						});
					});
			} else {
				// @todo use standardised error message as rejection reason
				reject({
					message: 'Web service failed',
					action: 'mausBasic',
					previousError: result
				});
			}
		});

		request.addEventListener('error', (event) => {
			reject({
				message: 'Unknown error - BAS server might be down or unreachable',
				action: 'mausBasic',
				previousError: event
			});
		});

		request.addEventListener('timeout', (event) => {
			reject({
				message: 'Timeout',
				action: 'mausBasic',
				previousError: event
			});
		});
		//
		//////////

		//////////
		// Send request
		//
		request.open('POST', serviceURL);
		request.overrideMimeType('text/xml');
		request.send(formData);
	});
}


/**
 * RESTfully call the WebMAUS General service, to automatically segment a
 * signal into words and phonemes based on an orthographic transcription.
 *
 * Please refer to the BAS Terms of Usage at
 * <https://clarin.phonetik.uni-muenchen.de/BASWebServices/#/help> and to the
 * service documentation at
 * <https://clarin.phonetik.uni-muenchen.de/BASRepository/WebServices/BAS_Webservices.cmdi.xml>.
 *
 * @param signal An audio signal to segment. Either as an AudioBuffer object
 *                or as an ArrayBuffer or Blob object containing a WAVE,
 *                NIST/SPHERE, or ALAW file.
 * @param text Transcription to accompany `signal`. Either as a string or as
 *              an ArrayBuffer or Blob object containing a file. In any
 *              case, it needs to be in BPF format (see service
 *              documentation for details).
 * @param language Language of `signal` and `text`. Can be a code like
 *                  'deu', see service documentation for a list of accepted
 *                  languages.
 * @param inputFileName When uploaded to the service (as upload files),
 *                       `text` and `signal` get a file name composed of
 *                       `inputFileName` and '.par' or '.wav', respectively.
 * @param options Additional options to be passed to the service. See service
 *                 documentation for a list of accepted options.
 * @param serviceURL The URL at which the REST API is located.
 * @returns A promise resolving to the requested segmentation.
 *
 * @todo this is a copy of mausBasic() - we still need to consider some differences.
 */
export function maus(signal:AudioBuffer | ArrayBuffer | Blob,
                     text:string | ArrayBuffer | Blob,
                     language:string,
                     inputFileName:string = 'browser-signal-processing',
                     options:Object = {},
                     serviceURL:string = 'https://clarin.phonetik.uni-muenchen.de/BASWebServices/services/runMAUS'):Promise<ArrayBuffer> {
	return new Promise((resolve, reject) => {
		//////////
		// Generate input file
		//
		var signalFile:Blob;
		var textFile:Blob;

		if (signal instanceof AudioBuffer) {
			var wave:ArrayBuffer = audioBufferToWav(signal);
			signalFile = new Blob([wave], {type: 'audio/x-wav'});
		} else if (signal instanceof ArrayBuffer) {
			signalFile = new Blob([signal], {type: 'audio/x-wav'})
		} else {
			signalFile = signal;
		}

		if (typeof text === 'string' || text instanceof ArrayBuffer) {
			textFile = new Blob([text], {type: 'text/plain'});
		} else {
			textFile = text;
		}
		//
		//////////

		//////////
		// Construct request
		//
		var formData = new FormData();

		formData.append('SIGNAL', signalFile, inputFileName + '.wav');
		formData.append('BPF', textFile, inputFileName + '.par');
		formData.append('LANGUAGE', language);

		for (var p in options) {
			if (options.hasOwnProperty(p)) {
				formData.append(p, options[p]);
			}
		}

		var request = new XMLHttpRequest();
		//
		//////////

		//////////
		// Event handlers
		//
		request.addEventListener('load', (event) => {
			var result:BASServiceResult = parseBASServiceResult(request.responseXML);

			if (result.success === true) {
				retrieveFile(result.downloadLink)
					.then((file) => {
						resolve(file);
					})
					.catch((reason) => {
						reject({
							message: 'Could not download result',
							action: 'maus',
							previousError: reason
						});
					});
			} else {
				// @todo use standardised error message as rejection reason
				reject({
					message: 'Web service failed',
					action: 'maus',
					previousError: result
				});
			}
		});

		request.addEventListener('error', (event) => {
			reject({
				message: 'Unknown error - BAS server might be down or unreachable',
				action: 'maus',
				previousError: event

			});
		});

		request.addEventListener('timeout', (event) => {
			reject({
				message: 'Timeout',
				action: 'maus',
				previousError: event
			});
		});
		//
		//////////

		//////////
		// Send request
		//
		request.open('POST', serviceURL);
		request.overrideMimeType('text/xml');
		request.send(formData);
	});
}
