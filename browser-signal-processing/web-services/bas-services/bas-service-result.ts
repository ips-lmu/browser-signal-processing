// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

/**
 * Collection of values returned by a number of BAS Services
 */
export interface BASServiceResult {
	success:boolean;
	downloadLink:string;
	output:string;
	warnings:string;
}

/**
 * Parse an XML response from an `XMLHttpRequest` to construct a
 * `BASServiceResult` object.
 * @param xml An XML document usually provided by `XMLHttpRequest.responseXML`.
 * @returns A `BASServiceResult` object with the values stored in `xml`.
 */
export function parseBASServiceResult(xml:Document):BASServiceResult {
	var result:BASServiceResult = {
		downloadLink: xml.documentElement.getElementsByTagName('downloadLink')[0].innerHTML,
		output: xml.documentElement.getElementsByTagName('output')[0].innerHTML,
		warnings: xml.documentElement.getElementsByTagName('warnings')[0].innerHTML,
		success: false,
	};

	if (xml.documentElement.getElementsByTagName('success')[0].innerHTML === 'true') {
		result.success = true;
	}

	return result;
}
