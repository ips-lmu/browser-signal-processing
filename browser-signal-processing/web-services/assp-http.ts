// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {audioBufferToWav} from "../formats/wave/audio-buffer-to-wav.function";
import {arrayBufferToBase64} from "../browser-api/base64";
import {arrayToAudioBuffer} from "../browser-api/format-conversion";

/**
 * The sample rate used by assp for pitch contour signals.
 * @type {number}
 */
export var asspPitchContourSampleRate:number = 1 / 0.005;


/**
 * Call a websocket service to receive a signal's pitch contour as given by the
 * algorithm of Kurt Schafer-Vincent.
 * @param signal The signal to analyse.
 * @param sampleRate The sampleRate of `signal`. Only needed when `signal`
 *                    is a number[] or Float32Array.
 * @param serviceURL The URL at which the service is located.
 * @returns A promise resolving to the requested pitch contour.
 */
export function asspKsvF0(signal:number[] | Float32Array | AudioBuffer | ArrayBuffer,
                          sampleRate:number = 0,
                          serviceURL:string = 'https://www.phonetik.uni-muenchen.de/apps/assp-http.php'):Promise<number[]> {
	return new Promise<number[]>((resolve, reject) => {
		//////////
		// Make sure we get signal in ArrayBuffer format
		//
		var waveFile:ArrayBuffer;

		if (signal instanceof ArrayBuffer) {
			waveFile = signal;
		} else if (signal instanceof AudioBuffer) {
			waveFile = audioBufferToWav(signal);
		} else {
			var buffer = arrayToAudioBuffer(signal, sampleRate);
			waveFile = audioBufferToWav(buffer);
		}
		//
		//////////

		//////////
		// base64 our signal
		//
		var base64encoded = arrayBufferToBase64(waveFile);
		//
		//////////

		//////////
		// Construct request
		//
		var formData = new FormData();

		formData.append('type', 'KSVF0');
		formData.append('signal', base64encoded);

		var request = new XMLHttpRequest();
		//
		//////////

		//////////
		// Event handlers
		//
		request.addEventListener('load', (event) => {
			if (Array.isArray(request.response)) {
				resolve(request.response);
			} else {
				reject({
					message: 'Failed to retrieve pitch track',
					action: 'asspKsvF0',
					previousError: request.response
				});
			}
		});

		request.addEventListener('error', (event) => {
			reject({
				message: 'Unknown error - assp-http server might be down or' +
				' unreachable',
				action: 'asspKsvF0',
				previousError: event
			});
		});

		request.addEventListener('timeout', (event) => {
			reject({
				message: 'Timeout',
				action: 'asspKsvF0',
				previousError: event
			});
		});
		//
		//////////

		//////////
		// Send request
		//
		request.responseType = 'json';
		request.open('POST', serviceURL);
		request.send(formData);
	});
}
