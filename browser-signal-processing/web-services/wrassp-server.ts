// (c) 2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

import {audioBufferToWav} from "../formats/wave/audio-buffer-to-wav.function";
import {arrayBufferToBase64} from "../browser-api/base64";
import {arrayToAudioBuffer} from "../browser-api/format-conversion";

/**
 * The sample rate used by wrassp for pitch contour signals.
 * @type {number}
 */
export var wrasspPitchContourSampleRate:number = 1 / 0.005;


/**
 * Call a websocket service to receive a signal's pitch contour as given by the
 * algorithm of Kurt Schafer-Vincent.
 * @param signal The signal to analyse.
 * @param sampleRate The sampleRate of `signal`. Only needed when `signal`
 *                    is a number[] or Float32Array.
 * @param serviceURL The URL at which the service is located.
 * @returns A promise resolving to the requested pitch contour.
 */
export function wrasspKsvF0(signal:number[] | Float32Array | AudioBuffer | ArrayBuffer, // @todo support Blob
                            sampleRate:number = 0,
                            serviceURL:string = 'ws://localhost:17891'):Promise<number[]> {
	return new Promise<number[]>((resolve, reject) => {
		//////////
		// Make sure we get signal in ArrayBuffer format
		//
		var waveFile:ArrayBuffer;

		if (signal instanceof ArrayBuffer) {
			waveFile = signal;
		} else if (signal instanceof AudioBuffer) {
			waveFile = audioBufferToWav(signal);
		} else {
			var buffer = arrayToAudioBuffer(signal, sampleRate);
			waveFile = audioBufferToWav(buffer);
		}
		//
		//////////

		//////////
		// base64 our signal and send it to the web service
		//
		var base64encoded = arrayBufferToBase64(waveFile);

		var request = {
			type: 'KSVF0',
			signal: base64encoded
		};

		var socket = new WebSocket(serviceURL);
		socket.onmessage = (event) => {
			var response = JSON.parse(event.data);
			resolve(response.data);
			socket.close();
		};

		socket.addEventListener('open', () => {
			socket.send(JSON.stringify(request));
		});

		socket.addEventListener('error', (event) => {
			reject({
				message: 'Unknown error - wrassp server might be down or' + ' unreachable',
				action: 'wrasspKsvF0',
				previousError: event
			});
		});
	});
}
