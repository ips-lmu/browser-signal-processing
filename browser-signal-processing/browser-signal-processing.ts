// (c) 2015-2016 Markus Jochim <markusjochim@phonetik.uni-muenchen.de>

/**
 * The browser-signal-processing module offers some signal processing functions
 * designed for speech applications.
 *
 * The module is organised into several directories:
 *
 * * basic: simple algorithms that fit into a few lines, such as RMS
 * * advanced: complex algorithms such as PSOLA
 * * web-services: algorithms executed via RESTful APIs or web socket
 *   connections rather than directly in the browser, such as WebMAUS
 * * formats: read and write specific on-disk formats, such as wave
 * * browser-api: helper functions to simplify the use of such APIs as
 *   WebAudio or XMLHttpRequest.
 *
 * The workflows submodule wraps some typical combinations of library
 * functions.
 *
 * @author Markus Jochim <markusjochim@phonetik.uni-muenchen.de>
 */

export * from "./browser-api/format-conversion";
export * from "./browser-api/base64";
export * from "./browser-api/retrieve-file.function";
export * from "./browser-api/audio-recorder.class";

export * from "./formats/textgrid/textgrid-service";
export * from "./formats/textgrid/textgrid-worker.class";
export * from "./formats/wave/audio-buffer-to-wav.function";

export * from "./basic/conversion-functions";
export * from "./basic/window-functions";
export * from "./basic/rms.function";

export * from "./advanced/psola";
export * from "./advanced/estimate-final-silence.function";

export * from "./web-services/assp-http";
export * from "./web-services/bas-services";
export * from "./web-services/wrassp-server";

export * from "./workflows";
