# 0.1.7 (2017-02-24)

- Now installing es6-shim via npm rather than via typings

# 0.1.6 (2017-02-24)

- Updated Typescript to 2.2.1
- Corrected code errors pointed out by the compiler

# 0.1.5 (2016-10-07)

- Release 0.1.4 on npmjs.com is broken

# 0.1.4 (2016-10-07)

- Added new web service to replace wrassp server
- Updated contribution guide with new URL

# 0.1.3 (2016-08-21)

- Removed `private` field from package.json for release on npmjs.com

# 0.1.2  (2016-08-21)

- Removed `typings install` from postinstall; it should now be possible to
  use this library as a dependency in other projects

# 0.1.1 (2016-08-21)

- Fixed build issues (missing files in build)

# 0.1.0 (2016-05-28)

- AudioRecorder: several improvements
- BAS web services: corrected wrong file name extensions that caused a severe
  server-side bug
- syllabify: pho2syl is now called with word-synchronous option enabled
- wrassp-server: Close WebSocket connections after the result has been received
- Documented build process in contribution guide

# 0.0.5 (2016-05-19)

- Added class AudioRecorder
- Added workflow psolaAdjustSegmentDurations()
- Added helper arrayBufferToString()
- Added helper arrayToAudioBuffer()
- Errors are now indicated in a standardised form in many places
- Allow more input formats for audio in wrasspKsvF0()
- Changed API exposing BAS services (library-internal; the services
  themselves were not changed)
- Some fixes
- Extended docs

# 0.0.4 (2016-04-17)

- Extended a lot on supported BAS web services

# 0.0.3 (2016-04-16)

- First properly documented build.
