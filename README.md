# Browser Signal Processing

The browser-signal-processing library offers some signal processing functions
designed for speech applications. Some of them are executed directly in the
Javascript engine, while others exploit remote web services.

The functions executed directly in the browser include both simple algorithms
that fit into a few lines, such as RMS, and advanced algorithms such as PSOLA.
The supported web services cover some very complex signal processing such as
automatic phoneme segmentation ([WebMAUS](https://clarin.phonetik.uni-muenchen.de/BASWebServices/)).

The library also includes some helper functions to read and write specific
on-disk formats (such as WAVE or TextGrid) and to simplify the use of such
browser APIs as WebAudio or XMLHttpRequest.

## Usage Hints

The signal processing functions either operate on an array of numbers
(number[]; the most basic form of a Javascript digital signal), or on a
Float32Array, or on an AudioBuffer object. Some functions need to know the
signal's sample rate. It needs to be passed explicitly unless the signal is
passed as an AudioBuffer object, which has the sample rate integrated.

Functions that need to exchange data in terms of some on-disk format (generally
functions that call remote web services) accept these data in two ways. They
can either be already encoded in said format and be passed as ArrayBuffer
objects (which are easily retrieved via XMLHttpRequests), or as Blob objects.
Or they can be in an unencoded form: string for plain text files,
AudioBuffer/Float32Array/number[] for audio files.

Functions that call remote web services either connect to them via WebSockets
or use asynchronous XMLHttpRequests to call RESTful APIs. The results are
made available to the calling Javascript/Typescript code by means of Promises
that resolve to the result values as soon as they are available.

## Usage Examples

@todo add usage examples

## License and Authors

The libary is published under the terms of the MIT license (see file LICENSE).

The main author is Markus Jochim (<markusjochim@phonetik.uni-muenchen.de>).

Some code is re-used from other projects, most notably the [EMU-WebApp](http://ips-lmu.github.io/EMU.html).
Contributors are:

* Raphael Winkelmann
* Georg Raess
* Matt Diamond
* Jam3
