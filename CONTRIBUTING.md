# Contributing to browser-signal-processing

Merge requests are always welcome. You can also contact the main author
Markus Jochim (<markusjochim@phonetik.uni-muenchen.de>) if you have any
questions.

Coding style has not been written down so far. Please try and use the style
present in the codebase. If that is inconistent, choose whichever you prefer.

The library is hosted at <https://gitlab.lrz.de/ips-lmu/browser-signal-processing>.
If you are a member of a Munich university, you already have an account at
the Leibniz-Rechenzentrum (LRZ) and can log in to gitlab.lrz.de. If not, you
have to ask the main author for an invitation to create an account.

## Getting started

Starting to change the library only takes these simple steps:

- Clone the repository
- Run `npm install`

## Build and release process

This is the place where I document my somewhat lengthy build and release
process until I get round to automating it.

This series of commands could basically be a shell script:

```bash

# First of all, make sure package.json (version!) and CHANGELOG are up-to-date.

# Clean build branch
git checkout build
git rm -r "*"
git commit -m "Cleaned for new build"

# Build
git checkout master
rm -r doc/
npm run build
npm run typedoc

# Copy relevant files to build branch
git checkout build
git checkout master package.json LICENSE CHANGELOG README.md CONTRIBUTING.md
git add browser-signal-processing.min.js browser-signal-processing.min.js.map doc/
find browser-signal-processing -name "*.js" -delete
find browser-signal-processing -name "*.js.map" -delete
find browser-signal-processing -name "npm-debug.log" -delete
mv browser-signal-processing/* .
git add browser-signal-processing.d.ts advanced/ basic/ browser-api/ formats/ web-services/ workflows.d.ts
git checkout master browser-signal-processing
mkdir ts
git mv browser-signal-processing ts

# Commit build
git commit -m "new build x.x.x"

# Add version tags to repo
git tag x.x.x
git checkout master
git tag src-x.x.x
```
